﻿using Microsoft.EntityFrameworkCore;
using WebBook.Models;

namespace WebBook.Data
{
    public class AplicationDBContext : DbContext
    {
        public AplicationDBContext(DbContextOptions<AplicationDBContext> options) : base(options)
        { 
            
        }

        public DbSet<EmprestimosModel> Emprestimos { get; set;}
    }
}
