﻿using Microsoft.AspNetCore.Mvc;
using WebBook.Data;
using WebBook.Models;

namespace WebBook.Controllers
{
    public class EmprestimosController : Controller
    {

        readonly private AplicationDBContext _context;

        public EmprestimosController(AplicationDBContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            IEnumerable<EmprestimosModel> emprestimos = _context.Emprestimos;
            return View(emprestimos);
        }
    }
}
